from django.contrib import admin

# Register your models here.
from core.models import User, Project, AssignProject
from core.models import Client

admin.site.register(User)
admin.site.register(Project)
admin.site.register(AssignProject)
admin.site.register(Client)