from django.db import models
from datetime import datetime
from core.models import Project

# Create your models here.
from django.conf import settings

USER = settings.AUTH_USER_MODEL


class Client(models.Model):
    client_name = models.CharField(max_length=100)
    created_at = models.DateTimeField(auto_now_add=True, blank=True)
    created_by = models.ForeignKey( 
        USER,
        on_delete=models.SET_NULL,
        null = True,
        related_name='created_by'
    )
    project_name = models.ForeignKey('Project', on_delete=models.CASCADE, related_name='project')

    def __str__(self):
        return self.client_name
