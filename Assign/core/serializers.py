from rest_framework import serializers
from .models import User, Project, Client, AssignProject
from rest_framework import exceptions

class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ['id', 'email', 'first_name', 'last_name', 'date_joined', 'is_active', 'is_staff', 'projects']

class ProjectSerializer(serializers.ModelSerializer):
    class Meta:
        model = Project
        fields = ['id', 'project_name']
        

class ClientSerializer(serializers.ModelSerializer):
    class Meta:
        model = Client
        fields = ['id', 'client_name', 'created_at', 'created_by', 'project_name']

class AssignProjectSerializer(serializers.ModelSerializer):
    class Meta:
        model = AssignProject
        fields = ['id', 'assign_to', 'projects']

from django.contrib.auth import authenticate, login

class LoginSerializer(serializers.Serializer):
    email = serializers.EmailField()
    password = serializers.CharField()

    def validate(self, data):
        email = data.get("email", "")
        password = data.get("password", "")

        if email and password:
            user = authenticate(email=email, password=password)
            if user:
                if user.is_active:  
                    data["user"] = user
                else:
                    msg = "user is deactivated"
                    raise exceptions.ValidationError(msg)
            else:
                msg = "Unable to Login With given Cradentials"
                raise exceptions.ValidationError(msg)
        else:
            msg = "provide email and password both"
            raise exceptions.ValidationError(msg)
        return data