from django.urls import path
from django.conf.urls import url
from .views import (UserRetrieveUpdateAPIView, UserListCreateAPIView, UserDestroyAPIView, UserListAPIView,
                    ProjectListCreateAPIView, ProjectRetrieveUpdateAPIView, ProjectDestroyAPIView, AssignProjectListCreateAPIView,
                    ClientRetrieveUpdateAPIView, ClientListCreateAPIView, ClientDestroyAPIView)
urlpatterns = [
    

    path('user/', UserListCreateAPIView.as_view(), name='createuser'),

    path('userlist/', UserListAPIView.as_view(), name='listalluser'),

    url(r'^(?P<pk>\d+)/reupuser$', UserRetrieveUpdateAPIView.as_view(), name='editusersinfo'),

    url(r'^(?P<pk>\d+)/desuser$', UserRetrieveUpdateAPIView.as_view(), name='destroyuserinfo'),

    
    path('project/', ProjectListCreateAPIView.as_view(), name='createproject'),

    url(r'^(?P<pk>\d+)/reupproject$', ProjectRetrieveUpdateAPIView.as_view(), name='editprojects'),

    url(r'^(?P<pk>\d+)/desproject$', ProjectDestroyAPIView.as_view(), name='destroyprojects'),


    path('assignpro/', AssignProjectListCreateAPIView.as_view(), name = 'assign'),


    path('client/', ClientListCreateAPIView.as_view(), name = 'client'),

    url(r'^(?P<pk>\d+)/reupclient$', ClientRetrieveUpdateAPIView.as_view(), name='editclientinfo'),
  
    url(r'^(?P<pk>\d+)/desclient$', ClientDestroyAPIView.as_view(), name='deleteclientinfo'),    
]
